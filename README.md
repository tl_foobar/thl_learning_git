# >_ Things Lab: Learning Git #

This is an example of basics of Git SCM (Source Code Management)

**Basics commands:**

```bash
git init            # creates a local repository

#
# To get this repository you should execute
# git clone https://aziflaj@bitbucket.org/aziflaj/thl_learning_git.git
#

git clone http://user@service/path/to/repo.git

git status          # check the status of a local repository
git add .           # stage this folder contents

git commit -m "Description here"        # make a commit, CTRL+S the project

git log             # check the history line of the repo


#
# Before pushing your local repo into an online git server 
# (like BitBucket or GitHub), you should add a remote repo.
# git remote add [remote_name] [remote_url]
#
git remote add origin http://user@service/path/to/repo.git

```

##More on Git
[This](https://try.github.io/levels/1/challenges/1) and [this](https://www.atlassian.com/git/tutorial/) are good online tutorials to learn Git