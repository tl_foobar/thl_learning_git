# This makefile helps us compile faster
# by executing only
#
# make
#
#instead of 
#
# gcc hello.c -o hello
#


hello: hello.c
	gcc hello.c -o hello

clean:
	rm -f hello *.o *.a 
