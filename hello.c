/**
 * This is a comment :)
 */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main(int argc,char** argv) {
	char answer;
	if (argc != 2) {
		fprintf(stderr,"usage: %s yourName\n",argv[0]);
		exit(1);
	}
	char *user = argv[1];
	if (!isupper(user[0])) {
		fprintf(stderr,"Your name should start with capital!\n");
		exit(2);
	}
	
	printf("Hello, %s!\n",user);
	printf("Did you have an exam today? ");
	scanf("%c",&answer);
	if (toupper(answer) == 'Y') {
		printf("I hope you pass that exam!\n");
	} else {
		printf("I am assuming that was a No...\n");
	}
	
	return (EXIT_SUCCESS);
}
